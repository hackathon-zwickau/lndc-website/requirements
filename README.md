# Requirements Development Guide

## Repo Rules
- always test your changes
- don't commit changes that break the build

## Development Setup
The requirements are written in LateX to make them referencable in Gitlab.
Please use this repository as the only source of knowledge.

To build the PDF your installation has to support a few LateX packages.
Most LateX installations are shipping a lot of packages out of the box, but some
might be missing. LateX will tell you at build time which packages are missing. 
Just Google the warning or error message and there will be many answers. 
You can find a full list of needed packages in the tex file with prefix '00_'.


Known Packages which have to be installed afterwards:
- texlive-babel-german (Fedora 30)
- texlive-hyphen-german (Fedora 30)
- texlive-roboto (Fedora 30)

### IDE
As IDE Eclipse and its LateX plugin 'Texlipse' is used. Every other IDE 
supporting LateX should be also fine. Please feel free to contribute your 
experiences with other IDEs.





